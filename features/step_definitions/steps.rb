Given /I'm signed in/ do
  visit '/c/sign_in'
  fill_in 'user_login', with: 'rfrodriguez1992@gmail.com'
  fill_in 'user_password', with: 'defaultpw'
  within :css, '.signinup_space' do
    click_on 'Sign in'
  end
end

When /I sign out from another window/ do
  within_window open_new_window do
    visit '/print/catalog'
    click_on 'Sign-Out'
  end
end

Then /I see a message/ do
  byebug
end
