require 'byebug'
require 'capybara'
require 'capybara/cucumber'
require 'rspec/expectations'
require 'active_support/all'

Capybara.configure do |config|
  config.default_driver = :selenium
  config.app_host   = 'http://localhost:3000'
end

World(Spec)
World(Capybara)
